package br.com.itau.filmes.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import br.com.itau.filmes.models.Ator;

@Service
public class AtorService {
	private ArrayList<Ator> atores;
	
	public AtorService() {
		atores = new ArrayList<>();
		
		Ator ator1 = new Ator();
		
		ator1.setId(1);
		ator1.setNome("Mussum");
		ator1.setIdade(50);
		ator1.setNacionalidade("Brasileiro");
		
		atores.add(ator1);
	}
	
	public ArrayList<Ator> obterAtores(){
		return atores;
	}
	
	public Ator obterAtor(int id) {
		for(Ator ator: atores) {
			if(ator.getId() == id) {
				return ator;
			}
		}
		
		return null;
	}
	
	public void inserirAtor(Ator ator) {
		atores.add(ator);
	}
}
